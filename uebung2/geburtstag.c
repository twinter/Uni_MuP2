#include <stdio.h>
#include <stdlib.h>

typedef struct { int day; int month; int year; } Geburtstag;

int months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int alterInTagen(Geburtstag const *date, int refYear) {
    // berechnet das Alter am 01.01. es gegebenen Jahres
    int days = date->day - 1;
    for (int i = 0; i < date->month; i++)
        days += months[i];
    days += (refYear - date->year) * 365;
    return days;
}

int myRand(int untereGrenze, int obereGrenze) {
    int r = rand() % (obereGrenze - untereGrenze + 1);
    r += untereGrenze;
    return r;
}

Geburtstag *randomGeburtstag(unsigned int len) {
    Geburtstag dates[len];
    for (int i = 0; i < len; i++) {
        Geburtstag d;
        d.year = myRand(1900, 2100);
        d.month = myRand(1, 12);
        d.day = myRand(1, months[dates[i].month - 1]);
        dates[i] = d;
    }
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreturn-stack-address"
    return dates;
#pragma clang diagnostic pop
}

void printGeburtstag(Geburtstag const *const dates, unsigned int len, int refYear) {
    for (int i = 0; i < len; i++) {
        int age = alterInTagen(&dates[i], refYear);
        if (age < 0)
            printf("Geboren am %d.%d.%d; Alter in Tagen bis zum Jahr %d: %d",
                    dates[i].day, dates[i].month, dates[i].year, refYear, age
            );
        else
            printf("Oh nooo, es ist das Jahr %d und ich werde erst in %d Tagen geboren am %d.%d.%d",
                   refYear, abs(age), dates[i].day, dates[i].month, dates[i].year
            );
    }
}

int compareGeburtstage(const void *geb1, const void *geb2) {
    int diff = alterInTagen(geb1, 2000) - alterInTagen(geb2, 2000);
    if (diff < 0)
        return -1;
    else if (diff > 0)
        return 1;
    return 0;
}

int main(int argc, char const *argv[]) {
    unsigned int len = 0;
    if (argc > 1)
        len = (unsigned int) strtol(argv[1], NULL, 10);
    if (len <= 0) {
        sprintf(stderr, "ungültiges Argument. Zahl größer als 0 übergeben");
        return EXIT_FAILURE;
    }

    int year;
    printf("Referenzjahr eingeben\n");
    if (scanf("%d", &year) == 0 | year < 1900 | year > 2100) {
        sprintf(stderr, "ungültige Eingabe, Zahl von 1900 bis 2100 eingeben.");
        return EXIT_FAILURE;
    }

    Geburtstag *birthdays;
    birthdays = randomGeburtstag(len);

    printf("Array vor der sortierung:");
    printGeburtstag(birthdays, len, year);

    qsort(birthdays, len, sizeof(Geburtstag), compareGeburtstage);
    printf("Array nach sortierung:");
    printGeburtstag(birthdays, len, year);

    return EXIT_SUCCESS;
}