

#include <stdlib.h>
#include <stdio.h>


void myAdd(int *a, int b) {
    *a += b;
}

void mySub(int *a, int *b) {
    *a -= *b;
}

void myMul(int a, int *b) {
    * b *= a;
}

void myDiv(int a, int b) {
    b /= a;
}

int main(void) {
    int *a = malloc(sizeof(int));
    int *b = malloc(sizeof(int));
    int c = 3;
    *a = 1;
    *b = 2;
    myAdd(a, *b);
    mySub(&c, b);
    myMul(*b, &c);
    myDiv(*a, c);
    myAdd(&c, *b - *a);
    mySub(b, a);
    myMul(*a, a);
    myDiv(c, *b);
    printf("%d\n", *a);
    printf("%d\n", *b);
    printf("%d\n", c);
    free(a);
    free(b);
}