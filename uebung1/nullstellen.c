#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    double a = 0, b = 0, c = 0;

    printf("Nullstellen der quadratischen Gleichung ax^2+bx+c=0\n");
    printf("Bitte geben Sie a, b, c ein: ");
    int success = scanf("%lf, %lf, %lf", &a, &b, &c);
    if (success != 3) {
        fprintf(stderr, "Fehler bei der Eingabe. Bitte geben Sie a und b "
                        "nochmals mit Komma getrennt ein.\n");
        return EXIT_FAILURE;
    }

    double diskriminante = pow(b, 2) - 4 * a * c;
    if (diskriminante < 0) {
        printf("Keine reellen Nullstelle");
    } else if (diskriminante == 0) {
        printf("Eine reelle Nullstelle: %lf", (-b) / (2 * a));
    } else {
        printf("Zwei reelle Nullstellen: %lf und %lf", (-b + diskriminante) / (2 * a), (-b - diskriminante) / (2 * a));
    }

    return EXIT_SUCCESS;
}
