#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct Dreieck {
	double seiteA;
	double seiteB;
	double seiteC;
};

int compareDouble(const void* a, const void* b) {
    double da = *((double*) a);
    double db = *((double*) b);
    return (int) (db - da);
}

int testLaenge(struct Dreieck tri) {
	double lengths[3] = {tri.seiteA, tri.seiteB, tri.seiteC};
	qsort(lengths, 3, sizeof(double), compareDouble);
	if (lengths[0] > lengths[1] + lengths[2])
	    return 0;
	return 1;
}

double umfang(struct Dreieck tri) {
    return tri.seiteA + tri.seiteB + tri.seiteC;
}

double flaeche(struct Dreieck tri) {
    double s = umfang(tri) / 2.;
    return sqrt(s * (s - tri.seiteA) * (s - tri.seiteB) * (s - tri.seiteC));
}

double umkreisRadius(struct Dreieck tri) {
    return (tri.seiteA * tri.seiteB * tri.seiteC) / (4 * flaeche(tri));
}

double inkreisRadius(struct Dreieck tri) {
    return (2 * flaeche(tri)) / umfang(tri);
}

// Winkel gegenüber von seiteC ist eingeschlossen von seiteA und seiteB
double kosinussatz(double seiteC, double seiteA, double seiteB) {
    double rad = acos((pow(seiteA, 2) + pow(seiteB, 2) - pow(seiteC, 2)) / (2 * seiteA * seiteB));
    return rad * (180 / M_PI);
}

void winkel(struct Dreieck tri, double *ptr_alpha, double *ptr_beta, double *ptr_gamma) {
    *ptr_alpha = kosinussatz(tri.seiteA, tri.seiteB, tri.seiteC);
    *ptr_beta = kosinussatz(tri.seiteB, tri.seiteC, tri.seiteA);
    *ptr_gamma = kosinussatz(tri.seiteC, tri.seiteA, tri.seiteB);
}

int main(void)
{
	struct Dreieck tri;
	double alpha, beta, gamma;

	printf("Geben Sie die Länge der Seiten a, b und c des Dreiecks an: ");
	int success = scanf("%lf, %lf, %lf", &tri.seiteA, &tri.seiteB, &tri.seiteC);
	if (success != 3) {
		fprintf(stderr, "Fehler bei der Eingabe: Erwarte 3 doubles durch Komma "
						"getrennt. Bitte nochmal!\n");
		return EXIT_FAILURE;
	}
	printf("Folgende Daten gelesen: a=%g, b=%g, c=%g\n", tri.seiteA, tri.seiteB,
		   tri.seiteC);

	if (testLaenge(tri) == 0) {
	    printf("Ungültige Eingabe! Unmögliches Dreieck eingegeben.");
        return EXIT_FAILURE;
	}

	printf("Der Umfang des Dreiecks ist: %f\n", umfang(tri));
	printf("Die Fläche des Dreiecks ist: %f\n", flaeche(tri));
	printf("Umkreisradius des Dreiecks ist: %f\n", umkreisRadius(tri));
	printf("Inkreisradius des Dreiecks ist: %f\n", inkreisRadius(tri));
	winkel(tri, &alpha, &beta, &gamma);
	printf("Der Winkel alpha des Dreiecks ist: %f\n", alpha);
	printf("Der Winkel beta des Dreiecks ist: %f\n", beta);
	printf("Der Winkel gamma des Dreiecks ist: %f\n", gamma);

	return EXIT_SUCCESS;
}
