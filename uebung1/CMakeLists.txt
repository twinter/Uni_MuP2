cmake_minimum_required(VERSION 3.10)
project(task2)

set(CMAKE_CXX_STANDARD 11)

add_executable(task2 aufgabe_2.c)

target_link_libraries(task2 m)
