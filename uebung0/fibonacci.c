#include <stdio.h>
#include <stdlib.h>

int main(void) {
    const int N = 1;

    if (N < 0)
        return EXIT_FAILURE;

    unsigned int f0 = 0, f1 = 1;

    for (unsigned int i = 0; i < N; i++) {
        unsigned int fib = f0 + f1;
        f0 = f1;
        f1 = fib;
    }

    printf("%u-te Fibonacci-Zahl: %u", N, f1);
    return EXIT_SUCCESS;
}