(defun fak (n)
	(if (< n 2)
		1
		(* 
			n
			(fak (- n 1))
		)
	)
)

(defun binom (n k)
	(/
		(fak n)
		(*
			(fak k)
			(fak (- n k))
		)
	)
)

(format T "5 über 3: ~D~%" (binom 5 3))
(format T "10 über 3: ~D~%" (binom 10 3))
(format T "12 über 4: ~D~%" (binom 12 4))
(format T "3382 über 1500: ~D~%" (binom 3382 1500))

; das Programm arbeitet bis a=3382 bei gültigem b korrekt da danach die maximal
; Rekursionstiefe überschritten wird. Der darstellbare Zahlenbereich ist also
; wesentlich größer als 64 bit. (Das Resultat des letzten Beispiels ist 3345 bit
; lang.)