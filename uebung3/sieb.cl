(defun filter_multiples (n new_list todo)
	; eine liste filtern nach einer zahl filtern
	(if (null todo)
		new_list
		(if (= (mod (car todo) n) 0)
			(filter_multiples n new_list (cdr todo))
			(filter_multiples 
				n 
				(append new_list (list (car todo)))
				(cdr todo)
			)
		)
	)
)

(defun _sieb (new_list todo)
	(if (null todo)
		new_list
		(_sieb
			(append new_list (list (car todo)))
			(filter_multiples (car todo) NIL (cdr todo))
		)
	)
)

(defun sieb (limit)
	(_sieb
		NIL
		(loop for i from 2 to limit collect i)
	)
)

(print (sieb 100))