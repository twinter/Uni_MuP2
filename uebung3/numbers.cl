(defun _sq (n sum)
	(setq digit (mod n 10))
	(if (< n 10)
		(+ sum (* n n))
		(_sq
			(truncate n 10)
			(+ sum (* digit digit))
		)
	)
)

(defun sq (n)
	(_sq n 0)
)

(defun f (n limit)
	(if (= n 1)
		T
		(if (<= limit 0)
			NIL
			(f (sq n) (- limit 1))
		)
	)
)

(loop for i from 1 to 100 do
	(if (f i 500)
		(format T "~D~%" i)
	)
)