(defun laenge (list)
	(if (null list)
		0
		(+ 
			1 
			(laenge (cdr list))
		)
	)
)

(format T "laenge (1 2 3 4 5) ~D~%" (laenge '(1 2 3 4 5)))
(format T "laenge ((1 2 3) (A B) (S ta X 2)) ~D~%" (laenge '((1 2 3) (A B) (S ta X 2))))
(format T "laenge () ~D~%" (laenge '()))