(defun differenz (a b)
	; a ohne b
	(if (null b)
		a
		(differenz (remove (car b) a) (cdr b))
	)
)

(defun vereinigung (a b)
	(append a (differenz b a))
)

(defun durchschnitt (a b)
	(differenz a (differenz a b))
)

(defun __potenzmenge (result position a)
	; position binär interpretiert zum zusammenbau einer potenzmenge
	(if (= position 0)
		result
		(if (= (mod position 2) 0)
			(__potenzmenge
				result
				(truncate position 2)
				(cdr a)
			)
			(__potenzmenge
				(append result (list (car a)))
				(truncate position 2)
				(cdr a)
			)
		)
	)
)

(defun _potenzmenge (result position a)
	; position ist interpretiert als binärzahl, beschreibt enthaltene mengenelemente
	(if (< position 0)
		result
		(_potenzmenge
			(append result (list (__potenzmenge NIL position a)))
			(- position 1)
			a
		)
	)
)

(defun potenzmenge (a)
	(_potenzmenge NIL (- (expt (list-length a) 2) 1) a)
)

(print "differenz")
(print (differenz '(0 1 2 3 4 5 6) '(2 5)))
(print (differenz '(0 1 2 3 4 5 6 7 8 9) '(0 2 3 4 6 7 8)))

(print "vereinigung")
(print (vereinigung '(0 1 2 3) '(2 3 4 5)))
(print (vereinigung '(0 2 4) '(1 3 5)))

(print "durchschnitt")
(print (durchschnitt '(0 1 2 3 4 5) '(2 3 4 5 6 7)))
(print (durchschnitt '(0 1 2 3 4 5 6 7 8 9) '(0 2 4 6 8)))

(print "potenzmenge")
(print (potenzmenge '(1 2 3)))
(print (potenzmenge '(a b c d e)))