#!/usr/bin/env python3

from dictDB import readFile, removeUser, printUser

if __name__ == '__main__':
	d = dict()
	readFile(d, 'dictionaryDB.txt')
	
	for id in list(d.keys()):
		if d[id]['job'] == 'Mathematiker':
			removeUser(d, id)
	
	for id in d.keys():
		if d[id]['age'] == 23:
			printUser(d, id)
