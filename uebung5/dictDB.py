#!/usr/bin/env python3
from typing import Dict, Any
import re

DictDB = Dict[int, Dict[str, Any]]


def addUser(dict_db: DictDB, string: str) -> None:
	m = re.match(r'(?P<id>\d+);(?P<name>[\w ]+);(?P<surname>[\w ]+);(?P<job>[\w ]+);(?P<age>\d+)', string)
	if m is None:
		return
	
	dict_db[int(m.group('id'))] = {
		'forename': m.group('name'),
		'surname': m.group('surname'),
		'job': m.group('job'),
		'age': int(m.group('age'))
	}


def readFile(dict_db: DictDB, file_name: str) -> None:
	with open(file_name, 'r') as f:
		for line in f.readlines():
			add_user(dict_db, line)
			
			
def removeUser(dict_db: DictDB, id: int) -> None:
	if id not in dict_db:
		return
	
	del dict_db[id]


def printUser(dict_db: DictDB, id: int) -> None:
	user = dict_db.get(id)
	if user is not None:
		print('{} {} ({}): {}'.format(user['forename'], user['surname'], user['age'], user['job']))


if __name__ == '__main__':
	d = dict(dict())
	read_file(d, 'dictionaryDB.txt')
	print_user(d, 12)
	remove_user(d, 12)
	print_user(d, 12)
	print_user(d, 8)
	print_user(d, 17)
