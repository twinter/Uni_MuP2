#!/usr/bin/env python3
import math
from typing import List


def quer(number: str) -> str:
	ret = 0
	for char in number:
		try:
			ret += int(char)
		except:
			pass
	
	return str(ret)


def gewichteteQuer(number: str) -> str:
	ret = 0
	i = 0
	for char in number[::-1]:
		mult = {0: 1, 1: 3, 2: 2}.get(i % 3)
		mult *= 1 if i % 6 < 3 else -1
		i += 1
		try:
			ret += int(char) * mult
		except:
			pass
	
	return str(ret)


def potenzierteQuer(number: str) -> str:
	ret = 0
	for char in number:
		try:
			ret += math.pow(int(char), len(number))
		except:
			pass
		
	return str(int(ret))


def alleArmstrongZahlen(number: int) -> List[int]:
	ret = []
	for i in range(number + 1):
		if int(potenzierteQuer(str(i))) == i:
			ret.append(i)
			
	return ret


if __name__ == '__main__':
	while True:
		number = input("Zahl eingeben:")
		print("Quersumme von {} ist {}".format(number, quer(number)))
		print("Gewichtete Quersumme von {} ist {}".format(number, gewichteteQuer(number)))
		print("Potenzierte Quersumme von {} ist {}".format(number, potenzierteQuer(number)))
		
		try:
			limit = int(number)
			if limit < 1000 or input('Armstrong zahlen wirklich berechnen? (yes/NO)')[0] in ['y', 'Y']:
				print("alle Armstrong Zahlen bis {}: {}".format(number, alleArmstrongZahlen(limit)))
		except ValueError:
			print('Armstrong Zahlen nicht berechnet: keine Zahl eingegeben')
		except IndexError:
			pass
