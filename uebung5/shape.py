#!/usr/bin/env python3
import math
import random
from typing import List


class Shape:
	def __init__(self, name: str):
		self.name = name
	
	def __repr__(self):
		return '{{{}: {}}}'.format(self.name, self.area())
	
	def __lt__(self, other):
		return self.area() < other.area()
	
	def area(self) -> float:
		raise NotImplementedError


class Square(Shape):
	def __init__(self, width: float):
		super().__init__('square')
		self.width = width
	
	def area(self) -> float:
		return math.pow(self.width, 2)


class Circle(Shape):
	def __init__(self, radius: float):
		super().__init__('circle')
		self.radius = radius
		
	def area(self):
		return math.pi * math.pow(self.radius, 2)


if __name__ == '__main__':
	shapes: List[Shape] = []
	
	# add squares
	for i in range(3):
		shapes.append(Square(
			(random.random() * 2 + 1) * math.sqrt(math.pi)
		))
		
	# add circles
	for i in range(3):
		shapes.append(Circle(
			random.random() * 2 + 1
		))
	
	print('Unsorted list:')
	print(shapes)
	
	shapes_sorted = sorted(shapes)
	
	print('\nSorted list:')
	print(shapes_sorted)
