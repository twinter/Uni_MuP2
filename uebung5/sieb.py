#!/usr/bin/env python3
from typing import List


def sieb(n: int) -> List[int]:
	base = [i for i in range(n + 1)]
	numbers = [i for i in range(2, n + 1)]
	i = 2
	
	while i < len(numbers):
		numbers = [x for x in numbers if x not in base[i * 2::i]]
		i += 1
	
	return numbers


if __name__ == '__main__':
	print('primzahlen von 2 bis {}: {}'.format(100, sieb(100)))
	print('primzahlen von 2 bis {}: {}'.format(1000, sieb(1000)))
	print('primzahlen von 2 bis {}: {}'.format(10000, sieb(10000)))
