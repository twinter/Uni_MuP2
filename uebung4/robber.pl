/* 5 Männer */
/* Eigenschaften: Name, Statur, Auto, Nationalität, Anzugfarbe, Begleiter */

menschen(X) :- 
	X = [_, _, _, _, _],
	member([jones, dick, vw, _, _, _], X), /* Mr. Jones ist dick und fährt einen VW */
	member([smith, _, jaguar, engländer, _, _], X), /* Mr. Smith fährt einen Jaguar und ist Engländer */
	member([_, dünn, fiat, _, _, _], X), /* Der dünne Mann fährt einen Fiat */
	member([poole, _, _, schotte, _, _], X), /* Mr. Poole ist Schotte */
	member([kent, _, _, _, blau, _], X), /* Mr. Kent trägt einen blauen Anzug */
	member([_, dick, _, _, schwarz, _], X), /* Der dicke Mann trägt einen schwarzen Anzug */
	member([_, dünn, _, ire, _, _], X), /* Der dünne Mann ist Ire */
	member([bright, _, renault, _, grün, _], X), /* Mr. Bright trägt einen grünen Anzug und fährt einen Renault */
	member([_, klein, _, schotte, _, _], X), /* Der kleine Mann ist Schotte */
	member([_, dünn, _, _, blau, _], X), /* Der dünne Mann trägt einen blauen Anzug */
	member([_, normal, _, kanadier, _, _], X), /* Der Mann normaler Statur ist Kanadier */
	member([_, _, mazda, _, grau, _], X), /* Der Mann im grauen Anzug fährt einen Mazda */
	member([_, _, _, waliser, schwarz, _], X), /* Der Mann im schwarzen Anzug ist Waliser */
	member([_, groß, _, _, braun, _], X), /* Der große Mann trägt einen braunen Anzug */
	member([A, _, _, ire, _, B], X), /*  der Ire war mit dem Kanadier zusammen */
	member([B, _, _, kanadier, _, A], X),
	member([C, groß, _, _, _, D], X), /*  der große Mann war mit dem Mazdafahrer zusammen */
	member([D, _, mazda, _, _, C], X).

räuber(R) :- menschen(X), member([R, _, _, _, _, allein], X).
