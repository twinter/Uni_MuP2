head(L, H) :- L = [H|_].

maxlist([], _) :- false.
maxlist(L, MAX) :- sort(0, @>, L, L_SORTED), head(L_SORTED, MAX).
