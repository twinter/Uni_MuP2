male(bernd).
male(klaus).
male(simon).
male(david).
male(hugo).
female(gabriele).
female(anna).
female(susi).
female(karin).
female(klara).
female(peter).

/* likes(X, Y) - X mag Y */
likes(gabriele, cats).
likes(anna, dogs).
likes(hugo, mate).
likes(susi, cats).
likes(simon, dogs).
likes(hugo, anna).

/* parent(X, Y) - X ist Elternteil von Y */
parent(anna, bernd).
parent(klaus, gabriele).
parent(klaus, hugo).
parent(klaus, susi).
parent(simon, david).
parent(simon, klara).
parent(karin, david).
parent(karin, hugo).
parent(karin, klara).
parent(hugo, bernd).


person(X) :- male(X); female(X).

/* X ist die Mutter von Y */
mother(X, Y) :- parent(X, Y), female(X).

/* X ist der Vater von Y */
father(X, Y) :- parent(X, Y), male(X).

/* X und Y haben ein Kind */
parents(X, Y) :- parent(X, A), parent(Y, A), not(X=Y).

/* X ist Großelternteil von Y */
grandparents(X, Y) :- parents(X, A), parents(A, Y).

/* X ist Geschwister von Y */
sibling(X, Y) :- parent(A, X), parent(A, Y), parent(B, X), parent(B, Y), not(X=Y).

/* X ist Halbgeschwister von Y */
halfSibling(X, Y) :- parent(A, X), parent(A, Y), not(X=Y).

/* X ist der Onkel von Y */
uncle(X, Y) :- parent(A, X), sibling(Y, A), male(X).

/* X ist die Tante von Y */
aunt(X, Y) :- parent(A, X), sibling(Y, A), female(X).

/* X mag jeden der das selbe mag wie es, aber nicht sich selbst */
like(X, Y) :- likes(X, A), likes(Y, A), not(X=Y).

/* David mag alle ohne Kinder */
like(david, Y) :- not(parent(Y, _)).
